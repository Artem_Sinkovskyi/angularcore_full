import { Injectable } from '@angular/core';
import { ivyEnabled } from '@angular/core/src/ivy_switch';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() { }
  public getUsers(): Array<any>{
    return [
        {firstName: 'Ivan', lastName: 'Ivanov'},
        {firstName: 'Peter', lastName: 'Petrov'},
        {firstName: 'Oleg', lastName: 'Olegovich'}
    ];
  }   
}
